<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Address;
use App\Entity\AddressObject;
use App\Enum\AddressObjectTypeCodeEnum;

class FullAddressResolver
{
    public function resolveFullAddress(Address $address): string
    {
        $addressObjectValues = [];
        $streetObjectTypeValue = null;
        $buildingNrOrHouseNrObjectTypeValue = null;
        $flatNrObjectTypeValue = null;

        /** @var AddressObject $addressObject */
        foreach ($address->getAddressObjects() as $addressObject) {
            $addressObjectValues[$addressObject->getAddressObjectType()->getSequenceNumber()] = $addressObject->getAddressObjectTypeValue()->getValue();

            if (AddressObjectTypeCodeEnum::STREET === $addressObject->getAddressObjectType()->getCode())
                $streetObjectTypeValue = $addressObject->getAddressObjectTypeValue();

            if (AddressObjectTypeCodeEnum::BUILDING_NR_OR_HOUSE_NAME === $addressObject->getAddressObjectType()->getCode())
                $buildingNrOrHouseNrObjectTypeValue = $addressObject->getAddressObjectTypeValue();

            if (
                AddressObjectTypeCodeEnum::FLAT_NR === $addressObject->getAddressObjectType()->getCode()
                || AddressObjectTypeCodeEnum::FLAT === $addressObject->getAddressObjectType()->getCode()
            )
                $flatNrObjectTypeValue = $addressObject->getAddressObjectTypeValue();
        }

        ksort($addressObjectValues);

        /* Handle full street name depending on if flat and house numbers are available */
        if ($streetObjectTypeValue && $buildingNrOrHouseNrObjectTypeValue && $flatNrObjectTypeValue) {
            $fullStreetName = sprintf('%s %s-%s', $streetObjectTypeValue->getValue(), $buildingNrOrHouseNrObjectTypeValue->getValue(), $flatNrObjectTypeValue->getValue());

            unset($addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);
            unset($addressObjectValues[$buildingNrOrHouseNrObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);
            unset($addressObjectValues[$flatNrObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);

            $addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()] = $fullStreetName;
        } else if ($streetObjectTypeValue && $buildingNrOrHouseNrObjectTypeValue) {
            $fullStreetName = sprintf('%s %s', $streetObjectTypeValue->getValue(), $buildingNrOrHouseNrObjectTypeValue->getValue());

            unset($addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);
            unset($addressObjectValues[$buildingNrOrHouseNrObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);

            $addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()] = $fullStreetName;
        } else {
            $fullStreetName = $streetObjectTypeValue->getValue();

            unset($addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()]);

            $addressObjectValues[$streetObjectTypeValue->getAddressObjectType()->getSequenceNumber()] = $fullStreetName;
        }

        ksort($addressObjectValues);

        return implode(', ', $addressObjectValues);
    }
}

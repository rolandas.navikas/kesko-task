<?php

declare(strict_types=1);

namespace App\Service\Manager;

use App\Entity\Order;
use App\Entity\OrderDeliveryType;
use App\Entity\OrderType;
use App\Enum\OrderDeliveryTypeCodeEnum;
use App\Enum\OrderTypeCodeEnum;
use App\Service\OrderReferenceGenerator;
use Doctrine\ORM\EntityManagerInterface;

readonly class OrderManager
{
    public function __construct(
        private EntityManagerInterface $em,
        private OrderReferenceGenerator $orderReferenceGenerator,
    ) {}

    public function generateOrder(): Order
    {
        $orderTypeEnums = OrderTypeCodeEnum::cases();
        $orderDeliveryTypeEnums = OrderDeliveryTypeCodeEnum::cases();

        $nextSequenceIndex = $this->em->getRepository(Order::class)->findNextSequenceIndex();
        $orderType = $this->em->getRepository(OrderType::class)->findOneBy(['code' => $orderTypeEnums[array_rand($orderTypeEnums)]]);
        $orderDeliveryType = $this->em->getRepository(OrderDeliveryType::class)->findOneBy(['code' => $orderDeliveryTypeEnums[array_rand($orderDeliveryTypeEnums)]]);

        $order = (new Order())
            ->setOrderType($orderType)
            ->setOrderDeliveryType($orderDeliveryType)
            ->setSequenceIndex($nextSequenceIndex)
        ;

        $order->setOrderReference($this->orderReferenceGenerator->generateOrderReference($order));

        $this->em->persist($order);

        $this->em->flush();

        return $order;
    }
}

<?php

declare(strict_types=1);

namespace App\Service\Manager;

use App\Dto\AddressDataDto;
use App\Entity\Address;
use App\Entity\AddressObject;
use App\Entity\AddressObjectType;
use App\Entity\AddressObjectTypeValue;
use App\Entity\AddressType;
use App\Enum\AddressTypeCodeEnum;
use App\Exception\InvalidAddressTypeException;
use App\Service\FullAddressResolver;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;

readonly class AddressManager
{
    public function __construct(
        private EntityManagerInterface $em,
        private FullAddressResolver $fullAddressResolver,
    ) {}

    public function createAddress(AddressDataDto $data): Address
    {
        if (!AddressTypeCodeEnum::tryFrom($data->getAddressType()))
            throw new InvalidAddressTypeException();

        $address = new Address();

        $this->populateAddressWithAddressData($address, $data);

        if (!$address->getAddressObjects()->isEmpty()) {
            $address->setFullAddress($this->fullAddressResolver->resolveFullAddress($address));
        }

        $this->em->persist($address);

        $this->em->flush();

        return $address;
    }

    public function updateAddress(Address $address, AddressDataDto $data): Address
    {
        if (!AddressTypeCodeEnum::tryFrom($data->getAddressType()))
            throw new InvalidAddressTypeException();

        /** @var AddressObject $addressObject */
        foreach ($address->getAddressObjects() as $addressObject) {
            $this->em->remove($addressObject->getAddressObjectTypeValue());
            $this->em->remove($addressObject);
        }

        $this->em->flush();

        $this->populateAddressWithAddressData($address, $data);

        if (!$address->getAddressObjects()->isEmpty()) {
            $address->setFullAddress($this->fullAddressResolver->resolveFullAddress($address));
        } else {
            $address->setFullAddress('');
        }

        $this->em->persist($address);

        $this->em->flush();

        return $address;
    }

    private function populateAddressWithAddressData(Address $address, AddressDataDto $data): void
    {
        $addressTypeEnum = AddressTypeCodeEnum::from($data->getAddressType());

        $addressType = $this->em->getRepository(AddressType::class)->findOneBy(['code' => $addressTypeEnum]);

        $address->setAddressType($addressType);

        /** @var array<int, AddressObjectType> $addressObjectTypes */
        $addressObjectTypes = $this->em->getRepository(AddressObjectType::class)->findByAddressTypeEnum($addressTypeEnum);

        $addressObjects = new ArrayCollection();
        foreach ($addressObjectTypes as $addressObjectType) {
            $value = $data->getPropertyByAddressObjectTypeCodeEnum($addressObjectType->getCode());

            if ($value) {
                $addressObjectTypeValue = new AddressObjectTypeValue(
                    value: $value,
                    addressObjectType: $addressObjectType,
                );

                $this->em->persist($addressObjectTypeValue);

                $addressObject = new AddressObject(
                    address: $address,
                    addressObjectType: $addressObjectType,
                    addressObjectTypeValue: $addressObjectTypeValue
                );

                $this->em->persist($addressObject);

                $addressObjects->add($addressObject);
            }
        }

        $address->setAddressObjects($addressObjects);
    }
}

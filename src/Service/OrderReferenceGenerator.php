<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Order;

class OrderReferenceGenerator
{
    public function generateOrderReference(Order $order): string
    {
        return sprintf('%s%s%07d', $order->getOrderType()->getCode()->value, $order->getOrderDeliveryType()->getCode()->value, $order->getSequenceIndex());
    }
}

<?php

declare(strict_types=1);

namespace App\Dto;

use App\Enum\AddressObjectTypeCodeEnum;

class AddressDataDto
{
    private ?string $country = null;
    private ?string $city = null;
    private ?string $county = null;
    private ?string $civilParish = null;
    private ?string $parish = null;
    private ?string $street = null;
    private ?string $buildingNumberOrHouseName = null;
    private ?string $district = null;
    private ?string $flatNumber = null;
    private ?string $flat = null;
    private ?string $municipality = null;
    private ?string $locality = null;
    private ?string $addressLine1 = null;
    private ?string $addressLine2 = null;
    private ?string $postalCode = null;
    private ?string $administrativeArea = null;
    private ?string $village = null;
    private ?string $settlement = null;
    private ?string $addressType = null;

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getDistrict(): ?string
    {
        return $this->district;
    }

    public function setDistrict(?string $district): self
    {
        $this->district = $district;

        return $this;
    }

    public function getCivilParish(): ?string
    {
        return $this->civilParish;
    }

    public function setCivilParish(?string $civilParish): self
    {
        $this->civilParish = $civilParish;

        return $this;
    }

    public function getParish(): ?string
    {
        return $this->parish;
    }

    public function setParish(?string $parish): self
    {
        $this->parish = $parish;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getBuildingNumberOrHouseName(): ?string
    {
        return $this->buildingNumberOrHouseName;
    }

    public function setBuildingNumberOrHouseName(?string $buildingNumberOrHouseName): self
    {
        $this->buildingNumberOrHouseName = $buildingNumberOrHouseName;

        return $this;
    }

    public function getFlatNumber(): ?string
    {
        return $this->flatNumber;
    }

    public function setFlatNumber(?string $flatNumber): self
    {
        $this->flatNumber = $flatNumber;

        return $this;
    }

    public function getFlat(): ?string
    {
        return $this->flat;
    }

    public function setFlat(?string $flat): self
    {
        $this->flat = $flat;

        return $this;
    }

    public function getMunicipality(): ?string
    {
        return $this->municipality;
    }

    public function setMunicipality(?string $municipality): self
    {
        $this->municipality = $municipality;

        return $this;
    }

    public function getLocality(): ?string
    {
        return $this->locality;
    }

    public function setLocality(?string $locality): self
    {
        $this->locality = $locality;

        return $this;
    }

    public function getAddressLine1(): ?string
    {
        return $this->addressLine1;
    }

    public function setAddressLine1(?string $addressLine1): self
    {
        $this->addressLine1 = $addressLine1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->addressLine2;
    }

    public function setAddressLine2(?string $addressLine2): self
    {
        $this->addressLine2 = $addressLine2;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function getAdministrativeArea(): ?string
    {
        return $this->administrativeArea;
    }

    public function setAdministrativeArea(?string $administrativeArea): self
    {
        $this->administrativeArea = $administrativeArea;

        return $this;
    }

    public function getVillage(): ?string
    {
        return $this->village;
    }

    public function setVillage(?string $village): self
    {
        $this->village = $village;

        return $this;
    }

    public function getSettlement(): ?string
    {
        return $this->settlement;
    }

    public function setSettlement(?string $settlement): self
    {
        $this->settlement = $settlement;

        return $this;
    }

    public function getAddressType(): ?string
    {
        return $this->addressType;
    }

    public function setAddressType(?string $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }

    public function getPropertyByAddressObjectTypeCodeEnum(AddressObjectTypeCodeEnum $addressObjectTypeCodeEnum): ?string
    {
        return match ($addressObjectTypeCodeEnum) {
            AddressObjectTypeCodeEnum::COUNTRY => $this->country,
            AddressObjectTypeCodeEnum::COUNTY => $this->county,
            AddressObjectTypeCodeEnum::CITY => $this->city,
            AddressObjectTypeCodeEnum::DISTRICT => $this->district,
            AddressObjectTypeCodeEnum::CIVIL_PARISH => $this->civilParish,
            AddressObjectTypeCodeEnum::PARISH => $this->parish,
            AddressObjectTypeCodeEnum::STREET => $this->street,
            AddressObjectTypeCodeEnum::BUILDING_NR_OR_HOUSE_NAME => $this->buildingNumberOrHouseName,
            AddressObjectTypeCodeEnum::FLAT_NR => $this->flatNumber,
            AddressObjectTypeCodeEnum::FLAT => $this->flat,
            AddressObjectTypeCodeEnum::POSTAL_CODE => $this->postalCode,
            AddressObjectTypeCodeEnum::MUNICIPALITY => $this->municipality,
            AddressObjectTypeCodeEnum::ADMINISTRATIVE_AREA => $this->administrativeArea,
            AddressObjectTypeCodeEnum::VILLAGE => $this->village,
            AddressObjectTypeCodeEnum::SETTLMENT => $this->settlement,
            AddressObjectTypeCodeEnum::LOCALITY => $this->locality,
            AddressObjectTypeCodeEnum::ADDRESS_LINE_1 => $this->addressLine1,
            AddressObjectTypeCodeEnum::ADDRESS_LINE_2 => $this->addressLine2,
        };
    }
}

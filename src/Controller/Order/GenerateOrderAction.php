<?php

declare(strict_types=1);

namespace App\Controller\Order;

use App\Service\Manager\OrderManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;

class GenerateOrderAction extends AbstractController
{
   public function __construct(
       private readonly OrderManager $orderManager,
   ) {}

    #[Route(path: '/generate-order', name: 'generate_order', methods: ['POST'])]
    public function __invoke(): Response
    {
        $this->orderManager->generateOrder();

        return new JsonResponse('Order generated successfully', Response::HTTP_CREATED);
    }
}

<?php

declare(strict_types=1);

namespace App\Controller\Address;

use App\Dto\AddressDataDto;
use App\Entity\Address;
use App\Exception\AddressNotFoundException;
use App\Service\Manager\AddressManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

class UpdateAddressAction extends AbstractController
{
    public function __construct(
        private readonly AddressManager $addressManager,
        private readonly EntityManagerInterface $em,
    ){}

    #[Route(path: '/address/{id}', name: 'update_address', methods: ['POST'])]
    public function __invoke(
        int $id,
        #[MapRequestPayload] AddressDataDto $addressDataDto
    ): Response
    {
        $address = $this->em->getRepository(Address::class)->find($id);

        if (!$address instanceof Address)
            throw new AddressNotFoundException();

        $this->addressManager->updateAddress($address, $addressDataDto);

        return new JsonResponse('Address updated successfully', Response::HTTP_OK);
    }
}

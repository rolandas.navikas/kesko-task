<?php

declare(strict_types=1);

namespace App\Controller\Address;

use App\Dto\AddressDataDto;
use App\Service\Manager\AddressManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Component\Routing\Attribute\Route;

class CreateAddressAction extends AbstractController
{
    public function __construct(
        private readonly AddressManager $addressManager,
    ) {}

    #[Route(path: '/address', name: 'create_address', methods: ['POST'])]
    public function __invoke(
        #[MapRequestPayload] AddressDataDto $addressDataDto
    ): Response
    {
        $this->addressManager->createAddress($addressDataDto);

        return new JsonResponse('Address created successfully', Response::HTTP_CREATED);
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'address_object_type_values')]
class AddressObjectTypeValue
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'aov_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'value', type: 'string', length: 500)]
    private string $value;

    #[ORM\ManyToOne(targetEntity: AddressObjectType::class)]
    #[ORM\JoinColumn(name: 'aot_id')]
    private AddressObjectType $addressObjectType;

    public function __construct(
        string $value,
        AddressObjectType $addressObjectType,
    )
    {
        $this->value = $value;
        $this->addressObjectType = $addressObjectType;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getAddressObjectType(): AddressObjectType
    {
        return $this->addressObjectType;
    }

    public function setAddressObjectType(AddressObjectType $addressObjectType): void
    {
        $this->addressObjectType = $addressObjectType;
    }
}

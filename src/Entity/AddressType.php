<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AddressTypeCodeEnum;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'address_types')]
class AddressType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'atp_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'code', length: 50, enumType: AddressTypeCodeEnum::class)]
    private AddressTypeCodeEnum $code;

    #[ORM\Column(name: 'name', type: 'string', length: 100)]
    private string $name;

    #[ORM\Column(name: 'country', type: 'string', length: 30)]
    private string $country;

    public function __construct(
        AddressTypeCodeEnum $code,
        string              $name,
        string              $country,
    )
    {
        $this->code = $code;
        $this->name = $name;
        $this->country = $country;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): AddressTypeCodeEnum
    {
        return $this->code;
    }

    public function setCode(AddressTypeCodeEnum $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }
}

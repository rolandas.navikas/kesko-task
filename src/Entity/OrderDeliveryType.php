<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\OrderDeliveryTypeCodeEnum;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'order_delivery_types')]
class OrderDeliveryType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'ord_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'code', length: 2, enumType: OrderDeliveryTypeCodeEnum::class)]
    private OrderDeliveryTypeCodeEnum $code;

    #[ORM\Column(name: 'name', type: 'string', length: 100)]
    private string $name;

    public function __construct(
        OrderDeliveryTypeCodeEnum $code,
        string $name,
    )
    {
        $this->code = $code;
        $this->name = $name;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): OrderDeliveryTypeCodeEnum
    {
        return $this->code;
    }

    public function setCode(OrderDeliveryTypeCodeEnum $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}

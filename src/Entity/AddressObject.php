<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'address_objects')]
class AddressObject
{
    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: Address::class, inversedBy: 'addressObjects')]
    #[ORM\JoinColumn(name: 'addr_id')]
    private Address $address;

    #[ORM\Id]
    #[ORM\ManyToOne(targetEntity: AddressObjectType::class)]
    #[ORM\JoinColumn(name: 'aot_id')]
    private AddressObjectType $addressObjectType;

    #[ORM\ManyToOne(targetEntity: AddressObjectTypeValue::class)]
    #[ORM\JoinColumn(name: 'aov_id')]
    private AddressObjectTypeValue $addressObjectTypeValue;

    public function __construct(
        Address $address,
        AddressObjectType $addressObjectType,
        AddressObjectTypeValue $addressObjectTypeValue,
    )
    {
        $this->address = $address;
        $this->addressObjectType = $addressObjectType;
        $this->addressObjectTypeValue = $addressObjectTypeValue;
    }

    public function getAddress(): Address
    {
        return $this->address;
    }

    public function setAddress(Address $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAddressObjectType(): AddressObjectType
    {
        return $this->addressObjectType;
    }

    public function setAddressObjectType(AddressObjectType $addressObjectType): self
    {
        $this->addressObjectType = $addressObjectType;

        return $this;
    }

    public function getAddressObjectTypeValue(): AddressObjectTypeValue
    {
        return $this->addressObjectTypeValue;
    }

    public function setAddressObjectTypeValue(AddressObjectTypeValue $addressObjectTypeValue): self
    {
        $this->addressObjectTypeValue = $addressObjectTypeValue;

        return $this;
    }
}

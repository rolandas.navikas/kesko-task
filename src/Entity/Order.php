<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrderRepository::class)]
#[ORM\Table(name: 'orders')]
class Order
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'ord_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'order_reference', type: 'string', length: 10)]
    private string $orderReference;

    #[ORM\Column(name: 'sequence_index', type: 'integer')]
    private int $sequenceIndex;

    #[ORM\ManyToOne(targetEntity: OrderDeliveryType::class)]
    #[ORM\JoinColumn(name: 'odt_id')]
    private OrderDeliveryType $orderDeliveryType;

    #[ORM\ManyToOne(targetEntity: OrderType::class)]
    #[ORM\JoinColumn(name: 'ot_id')]
    private OrderType $orderType;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrderReference(): string
    {
        return $this->orderReference;
    }

    public function setOrderReference(string $orderReference): self
    {
        $this->orderReference = $orderReference;

        return $this;
    }

    public function getSequenceIndex(): int
    {
        return $this->sequenceIndex;
    }

    public function setSequenceIndex(int $sequenceIndex): self
    {
        $this->sequenceIndex = $sequenceIndex;

        return $this;
    }

    public function getOrderDeliveryType(): OrderDeliveryType
    {
        return $this->orderDeliveryType;
    }

    public function setOrderDeliveryType(OrderDeliveryType $orderDeliveryType): self
    {
        $this->orderDeliveryType = $orderDeliveryType;

        return $this;
    }

    public function getOrderType(): OrderType
    {
        return $this->orderType;
    }

    public function setOrderType(OrderType $orderType): self
    {
        $this->orderType = $orderType;

        return $this;
    }
}

<?php

declare(strict_types=1);

namespace App\Entity;

use App\Enum\AddressObjectTypeCodeEnum;
use App\Repository\AddressObjectTypeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AddressObjectTypeRepository::class)]
#[ORM\Table(name: 'address_object_types')]
class AddressObjectType
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'aot_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'code', length: 50, enumType: AddressObjectTypeCodeEnum::class)]
    private AddressObjectTypeCodeEnum $code;

    #[ORM\Column(name: 'name', type: 'string', length: 100)]
    private string $name;

    #[ORM\Column(name: 'sequence_number', type: 'integer')]
    private int $sequenceNumber;

    #[ORM\ManyToOne(targetEntity: AddressType::class)]
    #[ORM\JoinColumn(name: 'atp_id')]
    private AddressType $addressType;

    public function __construct(
        AddressObjectTypeCodeEnum $code,
        string $name,
        int $sequenceNumber,
        AddressType $addressType,
    )
    {
        $this->code = $code;
        $this->name = $name;
        $this->sequenceNumber = $sequenceNumber;
        $this->addressType = $addressType;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCode(): AddressObjectTypeCodeEnum
    {
        return $this->code;
    }

    public function setCode(AddressObjectTypeCodeEnum $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSequenceNumber(): int
    {
        return $this->sequenceNumber;
    }

    public function setSequenceNumber(int $sequenceNumber): self
    {
        $this->sequenceNumber = $sequenceNumber;

        return $this;
    }

    public function getAddressType(): AddressType
    {
        return $this->addressType;
    }

    public function setAddressType(AddressType $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }
}

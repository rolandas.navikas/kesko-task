<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'addresses')]
class Address
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\SequenceGenerator(sequenceName: 'addr_seq')]
    #[ORM\Column(name: 'id', type: 'bigint', unique: true)]
    private int $id;

    #[ORM\Column(name: 'full_address', type: 'string', length: 1000)]
    private string $fullAddress;

    #[ORM\ManyToOne(targetEntity: AddressType::class)]
    #[ORM\JoinColumn(name: 'atp_id')]
    private AddressType $addressType;

    #[ORM\OneToMany(mappedBy: 'address', targetEntity: AddressObject::class)]
    private Collection $addressObjects;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getFullAddress(): string
    {
        return $this->fullAddress;
    }

    public function setFullAddress(string $fullAddress): self
    {
        $this->fullAddress = $fullAddress;

        return $this;
    }

    public function getAddressType(): AddressType
    {
        return $this->addressType;
    }

    public function setAddressType(AddressType $addressType): self
    {
        $this->addressType = $addressType;

        return $this;
    }

    public function getAddressObjects(): Collection
    {
        return $this->addressObjects;
    }

    public function setAddressObjects(Collection $addressObjects): self
    {
        $this->addressObjects = $addressObjects;

        return $this;
    }
}

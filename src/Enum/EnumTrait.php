<?php

declare(strict_types=1);

namespace App\Enum;

trait EnumTrait
{
    public static function isValid(mixed $enum): bool
    {
        return in_array($enum, static::getValues(), true);
    }

    /**
     * @return array<mixed, mixed>
     */
    public static function getValues(): array
    {
        return array_values(
            array_map(fn (self $enum) => $enum->value, self::cases())
        );
    }
}

<?php

declare(strict_types=1);

namespace App\Enum;

enum OrderDeliveryTypeCodeEnum: string implements EnumInterface
{
    use EnumTrait;

    case PHYSICAL_A = 'A';
    case PHYSICAL_B = 'B';
    case DELIVERY_DPD = '0';
    case DELIVERY_OMNIVA = '1';
}

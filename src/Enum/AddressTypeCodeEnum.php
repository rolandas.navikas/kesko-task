<?php

declare(strict_types=1);

namespace App\Enum;

enum AddressTypeCodeEnum: string implements EnumInterface
{
    use EnumTrait;

    case LV = 'LV';
    case LT = 'LT';
    case EE = 'EE';
    case EU = 'EU';
    case EE_PICKUP_POINTS_WO_POSTAL_CODE = 'EE_PICKUP_POINTS_WO_POSTAL_CODE';
    case LT_PICKUP_POINTS_WO_POSTAL_CODE = 'LT_PICKUP_POINTS_WO_POSTAL_CODE';
    case LV_PICKUP_POINTS_WO_POSTAL_CODE = 'LV_PICKUP_POINTS_WO_POSTAL_CODE';
}

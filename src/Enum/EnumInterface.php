<?php

declare(strict_types=1);

namespace App\Enum;

interface EnumInterface
{
    public static function isValid(mixed $enum): bool;

    public static function getValues(): array;
}

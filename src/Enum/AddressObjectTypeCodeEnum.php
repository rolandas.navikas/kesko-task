<?php

declare(strict_types=1);

namespace App\Enum;

enum AddressObjectTypeCodeEnum: string implements EnumInterface
{
    use EnumTrait;

    case COUNTRY = 'COUNTRY';
    case COUNTY = 'COUNTY';
    case CITY = 'CITY';
    case CIVIL_PARISH = 'CIVIL_PARISH';
    case PARISH = 'PARISH';
    case STREET = 'STREET';
    case DISTRICT = 'DISTRICT';
    case BUILDING_NR_OR_HOUSE_NAME = 'BUILDING_NR_OR_HOUSE_NAME';
    case FLAT_NR = 'FLAT_NR';
    case FLAT = 'FLAT';
    case POSTAL_CODE = 'POSTAL_CODE';
    case MUNICIPALITY = 'MUNICIPALITY';
    case ADMINISTRATIVE_AREA = 'ADMINISTRATIVE_AREA';
    case VILLAGE = 'VILLAGE';
    case SETTLMENT = 'SETTLMENT';
    case LOCALITY = 'LOCALITY';
    case ADDRESS_LINE_1 = 'ADDRESS_LINE_1';
    case ADDRESS_LINE_2 = 'ADDRESS_LINE_2';
}

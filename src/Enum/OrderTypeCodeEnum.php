<?php

declare(strict_types=1);

namespace App\Enum;

enum OrderTypeCodeEnum: string implements EnumInterface
{
    use EnumTrait;

    case DIGITAL = 'EP';
    case RETAIL = 'RR';
}

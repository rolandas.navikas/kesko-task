<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\OrderDeliveryType;
use App\Entity\OrderType;
use App\Enum\OrderDeliveryTypeCodeEnum;
use App\Enum\OrderTypeCodeEnum;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class OrderFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist(new OrderType(
            code: OrderTypeCodeEnum::DIGITAL,
            name: 'Digital'
        ));

        $manager->persist(new OrderType(
            code: OrderTypeCodeEnum::RETAIL,
            name: 'Retail'
        ));

        $manager->persist(new OrderDeliveryType(
            code: OrderDeliveryTypeCodeEnum::DELIVERY_DPD,
            name: 'DPD'
        ));

        $manager->persist(new OrderDeliveryType(
            code: OrderDeliveryTypeCodeEnum::DELIVERY_OMNIVA,
            name: 'Omniva'
        ));

        $manager->persist(new OrderDeliveryType(
            code: OrderDeliveryTypeCodeEnum::PHYSICAL_A,
            name: 'Physical shop A'
        ));

        $manager->persist(new OrderDeliveryType(
            code: OrderDeliveryTypeCodeEnum::PHYSICAL_B,
            name: 'Physical shop B'
        ));

        $manager->flush();
    }
}

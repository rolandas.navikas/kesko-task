<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AddressObjectType;
use App\Enum\AddressTypeCodeEnum;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class AddressObjectTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AddressObjectType::class);
    }

    /** @return array<int, AddressObjectType> */
    public function findByAddressTypeEnum(AddressTypeCodeEnum $addressTypeEnum): array
    {
        return $this->createQueryBuilder('aot')
            ->join('aot.addressType', 'atp')
            ->where('atp.code = :addressType')
            ->setParameter('addressType', $addressTypeEnum)
            ->getQuery()
            ->getResult();
    }
}

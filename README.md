# Tasks

1. SELECT aotv.* FROM address_object_type_values AS aotv \
   INNER JOIN address_object_types AS aot ON aotv.aot_id = aot.id \
   WHERE aot.code = 'CITY'; 

Swap ‘CITY’ for any other code found in address_object_types


2. SELECT addr.* FROM addresses AS addr \
   INNER JOIN pickup_points AS pp ON pp.addr_id = addr.id \
   INNER JOIN delivery_types AS dty ON dty.id = pp.dty_id \
   WHERE dty.type = 'PICKUP_POINT';



3. Actions **\App\Controller\Address\CreateAddressAction** and **\App\Controller\Address\UpdateAddressAction** 
   handle address creation. Send a POST request to the path **/address** to create, or **/address/{id}** to update 
   an existing one. Form data keys should be the ones found in **\App\Dto\AddressDataDto**



4. SELECT aot.code AS type, aotv.value AS value FROM address_object_type_values AS aotv \
  INNER JOIN address_object_types AS aot ON aotv.aot_id = aot.id;



5. Method **\App\Service\FullAddressResolver::resolveFullAddress** resolves the full address, based on the sequence number of the address object types


6. SELECT dfc.delivery_fee FROM delivery_fee_configuration AS dfc
   INNER JOIN address_types AS atp ON atp.id = dfc.atp_id
   INNER JOIN delivery_types AS dty ON dty.id = dfc.dty_id
   WHERE dty.code = 'DPD'
   AND atp.code = 'LT'
   AND (2 BETWEEN dfc.total_product_weight_from AND dfc.total_product_weight_to)
   AND (1 BETWEEN dfc.order_total_amount_from AND dfc.order_total_amount_to)

   Swap ‘DPD’ for any other code found in delivery_types
   Swap ‘LT’ for any other code found in address_types
   Swap the numbers in BETWEEN statements for different results based on weight and order total amount


7. UPDATE delivery_fee_configuration AS dfc
   SET delivery_fee = delivery_fee + 5
   FROM delivery_types AS dty
   WHERE dfc.dty_id = dty.id AND dty.code = 'DPD'

   Update the current configuration for DPD

   INSERT INTO delivery_fee_configuration (id, dty_id, dft_id, atp_id, aov_id, delivery_fee)
   SELECT
      (SELECT MAX(id) + 1 FROM delivery_fee_configuration),
      (SELECT id FROM delivery_types WHERE code = 'DPD'),
      (SELECT id FROM delivery_fee_types WHERE code = 'BASE'),
      (SELECT id FROM address_types WHERE code = 'LV'),
      (
         SELECT aotv.id
         FROM address_object_type_values AS aotv
         INNER JOIN address_object_types AS aot ON aot.id = aotv.aot_id
         INNER JOIN address_types AS atp ON atp.id = aot.atp_id
         WHERE aot.code = 'CITY' AND atp.code = 'LV' AND aotv.value = 'Rīga'
      ),
      3.00

      Insert new configuration for a delivery fee with delivery type DPD and city Rīga


8. INSERT INTO delivery_fee_types (id, code, name)
   SELECT
      (SELECT MAX(id) + 1 FROM delivery_fee_types),
      'SATURDAY',
      'Saturday'

   Add a new delivery fee type for Saturday

   INSERT INTO delivery_fee_configuration (id, dty_id, dft_id, delivery_fee)
SELECT
(SELECT MAX(id) + 1 FROM delivery_fee_configuration),
(SELECT id FROM delivery_types WHERE code = 'DPD'),
(SELECT id FROM delivery_fee_types WHERE code = 'SATURDAY'),
3.00


Add a delivery fee for DPD with delivery type Saturday


9. SELECT COUNT(pp.id) AS pickup_point_count, aov.value AS city FROM pickup_points AS pp
INNER JOIN address_objects AS ao ON ao.addr_id = pp.addr_id
INNER JOIN address_object_type_values AS aov ON aov.id = ao.aov_id
INNER JOIN address_object_types AS aot ON aot.id = ao.aot_id
INNER JOIN delivery_types AS dty ON dty.id = pp.dty_id
WHERE dty.code = 'OMNIVA_PICKUP' AND aot.code = 'CITY'
GROUP BY aov.value


10. INSERT INTO delivery_fee_configuration (id, dty_id, dft_id, atp_id, total_product_weight_to, delivery_fee)
SELECT
(SELECT MAX(id) + 1 FROM delivery_fee_configuration),
(SELECT id FROM delivery_types WHERE code = 'DPD'),
(SELECT id FROM delivery_fee_types WHERE code = 'BASE'),
(SELECT id FROM address_types WHERE code = 'LT'),
9.99,
10


INSERT INTO delivery_fee_configuration (id, dty_id, dft_id, atp_id, total_product_weight_from, delivery_fee)
SELECT
(SELECT MAX(id) + 1 FROM delivery_fee_configuration),
(SELECT id FROM delivery_types WHERE code = 'DPD'),
(SELECT id FROM delivery_fee_types WHERE code = 'BASE'),
(SELECT id FROM address_types WHERE code = 'LT'),
10,
20


11. UPDATE contacts SET phone_number = REPLACE(phone_number, '+371', '') WHERE phone_number LIKE '+371%'; \
UPDATE contacts SET phone_number = REPLACE(phone_number, '00371', '') WHERE phone_number LIKE '00371%'; \
UPDATE contacts SET phone_number = REPLACE(phone_number, '371 ', '') WHERE phone_number LIKE '371 %'; \
UPDATE contacts SET phone_number = REPLACE(phone_number, '371', '') WHERE phone_number LIKE '371%';


12. DELETE FROM
contacts AS a
USING contacts AS b
WHERE
a.id < b.id
AND a.phone_number = b.phone_number;


13. Method **\App\Service\OrderReferenceGenerator::generateOrderReference** generates an order reference number next in the sequence, based on the order delivery type and order type.


## Set-up if needed
1. Run `docker compose build --no-cache` to build fresh images
2. Run `docker compose up --pull always -d --wait` to start the project
3. Add the .env.local file with overridden values to suit your development environments connection to the database
4. Run `docker compose run --rm php comopser install` to install the required packages
5. Run `docker compose run --rm php bin/console doctrine:migrations:migrate` to update the database with order tables
6. Run `docker compose run --rm php bin/console doctrine:fixtures:load --append` to populate database order_types and order_delivery_types tables


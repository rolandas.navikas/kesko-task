<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20231215102728 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE ord_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE order_delivery_types (id BIGINT NOT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE order_types (id BIGINT NOT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(100) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE orders (id BIGINT NOT NULL, odt_id BIGINT DEFAULT NULL, ot_id BIGINT DEFAULT NULL, order_reference VARCHAR(10) NOT NULL, sequence_index INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E52FFDEE598FE648 ON orders (odt_id)');
        $this->addSql('CREATE INDEX IDX_E52FFDEEA01D3C68 ON orders (ot_id)');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE598FE648 FOREIGN KEY (odt_id) REFERENCES order_delivery_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEA01D3C68 FOREIGN KEY (ot_id) REFERENCES order_types (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE ord_seq CASCADE');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEE598FE648');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEEA01D3C68');
        $this->addSql('DROP TABLE order_delivery_types');
        $this->addSql('DROP TABLE order_types');
        $this->addSql('DROP TABLE orders');
    }
}
